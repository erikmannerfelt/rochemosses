import pandas as pd
import geopandas as gpd
from pathlib import Path
import rasterio as rio
import rasterio.warp
import numpy as np
import sklearn.pipeline
import sklearn.linear_model
import sklearn.preprocessing
import matplotlib.pyplot as plt
import matplotlib as mpl
import xarray as xr
from tqdm.contrib.concurrent import process_map
from typing import Generator
from multiprocessing import Pool
from tqdm import tqdm, trange
import scipy.optimize
import functools
import json


def get_locations() -> dict[str, tuple[float, float]]:
    return {
        "bassen": (524500., 8683081.),
    }

def get_regions() -> dict[str, rio.coords.BoundingBox]:
    return {
        "nordenskiold_land": rio.coords.BoundingBox(500830, 8636930, 561810, 8701030),
        "nl_bigger": rio.coords.BoundingBox(453810,8625130, 612850, 8717550),
    }


def get_periods() -> dict[str, tuple[float, float]]:
    return {
        "HOL": (11700, 4200),
        "ELH": (4200, 2500),  # Early Late Holocene (for the lack of a better name)
        "RWP": (2500, 1650),  # Roman Warm Period
        "DACP": (1650, 1000),  # Dark Ages Cold Period
        "MCA": (1000, 700), # Medieval Climate Anomaly
        "LIA": (700, 0)  # Little Ice Age
    }

def classify_ages(ages: pd.Series) -> pd.Series:

    periods = get_periods()
    edges = [p[1] for p in periods.values()]
    names = np.array(list(periods.keys()))

    return names[np.digitize(ages, edges)]
    


def read_and_format_db(db_path: Path = "input/VEGLAS database updated.xlsx", coords_2021_path = "input/coordinates_2021.geojson", dem_path: Path = "input/S0_DTM20.tif"):
    dates = (
        pd.read_excel(db_path)
        .set_index("Database ID")
        )
    dates.index = dates.index.astype(str)

    coords_2021 = gpd.read_file(coords_2021_path).set_index("name")
    coords_2021["Latitude"] = coords_2021.geometry.y 
    coords_2021["Longitude"] = coords_2021.geometry.x 
    dates.loc[dates["Field ID"].isin(coords_2021.index), ["Latitude", "Longitude", "Distance to ice margin (m)"]] = coords_2021[["Latitude", "Longitude", "margin_distance"]].values

    dates = gpd.GeoDataFrame(dates, geometry=gpd.points_from_xy(dates["Longitude"], dates["Latitude"], crs=4326)).to_crs(32633)

    with rio.open(dem_path) as raster:
        dates["elevation"] = np.fromiter(raster.sample(np.transpose([dates.geometry.x, dates.geometry.y])), dtype=raster.dtypes[0], count=dates.shape[0])

    dates.rename(inplace=True, columns={"Median callibrated age BP 2σ": "age", "Error + 2σ": "age_err_upper", "Error -2σ": "age_err_lower"})

    dates["period"] = classify_ages(dates["age"])
    dates["easting"] = dates.geometry.x
    dates["northing"] = dates.geometry.y
    
    return dates[["age", "age_err_upper", "age_err_lower", "period", "elevation", "easting", "northing", "geometry"]]


def perturb_dates(dates: pd.DataFrame, loc_err: float, elev_err: float, random_seed: int | None = None, subsample: float | None = None) -> pd.DataFrame:
    dates = dates.copy()

    if subsample is not None:
        dates = dates.sample(frac=subsample, random_state=random_seed)

    rng: np.random.Generator = np.random.default_rng(random_seed)

    dates["age"] += rng.normal(loc=dates["age_err_upper"] - dates["age_err_lower"], scale=dates[["age_err_upper", "age_err_lower"]].mean(axis=1) / 2)

    dates[["easting", "northing"]] += rng.normal(scale=loc_err, size=(dates.shape[0], 2))

    dates["elevation"] += rng.normal(scale=elev_err, size=dates.shape[0]) 

    return dates


def generate_perturbations(dates: pd.DataFrame, count_per_period: int,  loc_err: float, elev_err: float, subsample: float, random_seed: int | None = None) -> Generator[pd.DataFrame, None, None]:
    for i, (_, per_period) in enumerate(dates.groupby("period", as_index=False)):

        for j in range(count_per_period):
            new_random_seed = random_seed + i + j if random_seed is not None else None
            yield perturb_dates(per_period, loc_err=loc_err, elev_err=elev_err, subsample=subsample, random_seed=new_random_seed)

def prediction_function(xvals, coefs) -> np.ndarray:
    xvals = np.ascontiguousarray(xvals)

    degree = (len(coefs) - 2) // 2
    yvals = coefs[-2] * xvals[:, -1] + coefs[-1]

    for i in range(0, degree + 1):
        for j in range(xvals.shape[1] - 1):
            yvals += coefs[i + (degree * j)] * xvals[:, j] ** (i + 1)

    return yvals

def _prediction_cost(coefs, xvals, yvals):
    return yvals - prediction_function(xvals, coefs)

def _leastsq_fit(xvals, yvals, random_state: int, degree: int):
    rng: np.random.Generator = np.random.default_rng(random_state)
    guess = rng.normal(scale=0.3, size=degree * 2 + 2)
    guess[-2] = np.abs(guess[-2])
    return scipy.optimize.least_squares(
        functools.partial(_prediction_cost, xvals=np.ascontiguousarray(xvals), yvals=np.ascontiguousarray(yvals)),
        #np.zeros(degree * 2 + 2)
        guess,
        bounds = ([-np.inf] * (degree * 2) + [0., -np.inf], np.inf),
        #bounds=[Bounds()] * degree + [Bounds(-np.inf, 0.), Bounds()]
    )

def fit_date_trend(dates: pd.DataFrame, fit_cols: list[str], pred_col: str, degree: int, random_state: int): 
    retval = _leastsq_fit(xvals=dates[fit_cols].values, yvals=dates[pred_col].values, random_state=random_state, degree=degree)

    return {f"coef_{str(i).zfill(2)}": value for i, value in enumerate(retval.x)} | {"cost": retval.cost, "period": ",".join(dates["period"].unique())}

class Normalizer():

    def __init__(self, minval: float, maxval: float):
        self.minval = float(minval)
        self.maxval = float(maxval)

    @classmethod
    def from_series(cls, series: pd.Series):
        return cls(minval=series.min(), maxval=series.max())

    @classmethod
    def from_json(cls, string: str):
        return cls.from_dict(json.loads(string))

    def to_dict(self) -> dict[str, float]:
        return {"minval": self.minval, "maxval": self.maxval}

    @classmethod
    def from_dict(cls, minmax: dict[str, float]):
        return cls(minval=float(minmax["minval"]), maxval=float(minmax["maxval"]))
    
    def __call__(self, data: np.ndarray | pd.Series | pd.DataFrame):
        return (data - self.minval) / (self.maxval - self.minval)

    def scale(self, data: np.ndarray | pd.Series | pd.DataFrame):
        return data / (self.maxval - self.minval)

    def inverse(self, data: np.ndarray | pd.Series | pd.DataFrame):
        return (data * (self.maxval - self.minval)) + self.minval


def generate_fit_distribution(dates: pd.DataFrame, n_iters: int = int(1e5), random_state: int = 1, degree: int = 10, fit_cols: list["str"] = ["easting", "northing", "age"], pred_col: str = "elevation"): 
    cache_dir = Path(".cache")
    cache_path = cache_dir.joinpath(f"run_{n_iters}_{random_state}_{degree}.nc")

    if cache_path.is_file():
        res = xr.open_dataset(cache_path)
        for variable in res.variables:
            if str(res[variable].dtype) == "object":
                res[variable] = res[variable].astype(str)
        return res

    cache_dir.mkdir(exist_ok=True)

    all_cols = fit_cols + [pred_col]
    total_iters = dates["period"].unique().shape[0] * n_iters

    norms = {col: Normalizer.from_series(dates[col]) for col in all_cols}

    norm_df = pd.DataFrame({col: norms[col](dates[col]) for col in all_cols})
    norm_df[["age_err_upper", "age_err_lower"]] = norms["age"].scale(dates[["age_err_upper", "age_err_lower"]])
    norm_df["period"] = dates["period"].values

    loc_err = norms["easting"].scale(10)
    elev_err = norms["elevation"].scale(2)
    
    perturbations = generate_perturbations(dates=norm_df, count_per_period=n_iters, random_seed=random_state, loc_err=loc_err, elev_err=elev_err, subsample=0.5)

    run_fit = functools.partial(fit_date_trend, fit_cols=fit_cols, pred_col=pred_col, random_state=random_state, degree=degree) 

    result = []
    with Pool(processes=7) as pool, tqdm(total=total_iters) as progress_bar:
        for retval in pool.imap_unordered(run_fit, perturbations):
            result.append(retval)
            progress_bar.update()
            
    res_raw = xr.Dataset.from_dataframe(pd.DataFrame(result))
    res = res_raw[[col for col in res_raw.data_vars if "coef_" in col]].to_array("c").to_dataset(name="coefs")

    for variable in res_raw.data_vars:
        if str(variable).startswith("coef_"):
            continue
        res[variable] = res_raw[variable]

    for variable in res.variables:
        if str(res[variable].dtype) == "object":
            res[variable] = res[variable].astype(str)
    res.attrs.update({col: json.dumps(norms[col].to_dict()) for col in norms})
    res.to_netcdf(cache_path)#, encoding={key: {    

    return res
    

def get_location_time_series(distribution: xr.Dataset, easting: float = 524500., northing: float = 8683081., resolution: int = 500):

    # Remove fits where the trend in time was extremely close to zero
    #distribution = distribution.where((distribution["coefs"].isel(c=-2) > 1e-5).reset_coords(drop=True), drop=True)

    # For some reason, the filtering above resets some str types to object
    for variable in distribution.variables:
        if str(distribution[variable].dtype) == "object":
            distribution[variable] = distribution[variable].astype(str)

    norms = {key: Normalizer.from_json(value) for key, value in distribution.attrs.items()}

    period_intervals = get_periods()

    age_bins = np.linspace(min(min(p) for p in period_intervals.values()), max(max(p) for p in period_intervals.values()), num=resolution, dtype=float)
    ela_bins = np.linspace(0., 1700., num=resolution)


    histogram = np.zeros((ela_bins.size - 1, age_bins.size - 1))
     
    for period, period_data in tqdm(distribution.groupby("period"), desc="Binning ELAs"):

        max_time, min_time = get_periods()[period]

        times = norms["age"](age_bins[(age_bins >= min_time) & (age_bins <= max_time)])

        pred_df = pd.DataFrame({"easting": np.repeat(easting, times.shape[0]), "northing": np.repeat(northing, times.shape[0]), "age": times}).astype(float)


        steps = np.r_[np.arange(0, period_data["index"].shape[0], step=5000, dtype=int), [period_data["index"].shape[0]]]

        for i in range(1, steps.size):
            subset = period_data.isel(index=slice(steps[i - 1], steps[i])) 

            pred = np.apply_along_axis(
                lambda coefs, *_: prediction_function(xvals=pred_df, coefs=coefs),
                axis=0,
                arr=subset["coefs"].values
            )

            histogram += np.histogram2d(
                norms["elevation"].inverse(pred.ravel()),
                norms["age"].inverse(np.repeat(times, pred.shape[1])),
                bins=[ela_bins, age_bins]
            )[0] 


    histogram[histogram < 50] = np.nan

    hist_log = np.log10(histogram)

    plt.imshow(hist_log, extent=[age_bins[0], age_bins[-1], ela_bins[-1], ela_bins[0]], aspect="auto")
    plt.ylim(plt.gca().get_ylim()[::-1])
    plt.xlabel("Age (yrs BP)")
    plt.ylabel("ELA (m a.s.l.)")
    plt.show()

def show_map(distribution: xr.Dataset, bounds: rio.coords.BoundingBox, age: float, height: int = 100, width: int =100):
    period_intervals = get_periods()
    norms = {key: Normalizer.from_json(value) for key, value in distribution.attrs.items()}

    period = ""
    for key in period_intervals:
        if period_intervals[key][0] < age:
            continue
        if period_intervals[key][1] > age:
            continue

        period = key

    with rio.open("input/S0_DTM20.tif") as raster:
        window = rio.windows.from_bounds(*bounds, transform=raster.transform)
        transform = rio.windows.transform(window, raster.transform)

        res = raster.res[0]

        dem_highres = raster.read(1, window=window, masked=True).filled(np.nan)
        dem = np.empty((height, width), dtype="float32")
        crs = raster.crs

        dem = rasterio.warp.reproject(
            dem_highres,
            destination=dem,
            src_crs=crs,
            dst_crs=crs,
            src_transform=transform,
            dst_transform=rio.transform.from_bounds(*bounds, width, height),
            resampling=rasterio.warp.Resampling.bilinear
        )[0]

        slope_ish = np.sqrt(np.sum(np.square(np.gradient(dem_highres)), axis=0)) / res

    easting_coords = np.linspace(bounds.left + res / 2, bounds.right - res / 2, num=width)

    northing_coords = np.linspace(bounds.bottom  + res / 2, bounds.top - res / 2, num=height)[::-1]

    eastings, northings = np.meshgrid(easting_coords, northing_coords)

    indices = np.arange(0, eastings.size).reshape(eastings.shape).T.ravel()
    pred_df = pd.DataFrame({"easting": norms["easting"](eastings.ravel()), "northing": norms["northing"](northings.ravel()), 
        "age": norms["age"](np.repeat(age, eastings.size))}) 

    ela_bins = np.arange(0., 1700., step=3)
    index_bins = np.unique(np.r_[indices, [indices.max() + 1]])

    distribution = distribution.where(distribution["period"] == period, drop=True)
  
    steps = np.r_[np.arange(0, distribution["index"].shape[0], step=5000, dtype=int), [distribution["index"].shape[0]]]

    histogram = np.zeros((ela_bins.shape[0] - 1,) + eastings.shape, dtype=int)

    elas_reshaped = np.repeat((ela_bins[1:] - np.diff(ela_bins) / 2).reshape(-1, 1), eastings.size, axis=1).reshape(histogram.shape)

    for i in trange(1, steps.size):
        subset = distribution.isel(index=slice(steps[i - 1], steps[i])) 

        pred = np.apply_along_axis(
            lambda coefs, *_: prediction_function(xvals=pred_df, coefs=coefs),
            axis=0,
            arr=subset["coefs"].values
        )

        histogram += np.histogram2d(
            norms["elevation"].inverse(pred.ravel()),
            np.repeat(indices, pred.shape[1]),
            bins=[ela_bins, index_bins],
        )[0].reshape(histogram.shape).astype(int)
    

    elas = np.average(elas_reshaped, axis=0, weights=histogram)

    elas_highres = slope_ish.copy()

    elas_highres = rasterio.warp.reproject(
        elas,
        destination=elas_highres,
        src_crs=crs,
        dst_crs=crs,
        dst_transform=transform,
        src_transform=rio.transform.from_bounds(*bounds, width, height),
        resampling=rasterio.warp.Resampling.bilinear
    )[0]

    def std_weighted(values, weights, axis=0):
        return np.sqrt(np.mean(np.square(np.abs(values - np.average(values, axis=axis, weights=weights))), axis=axis))

    elas_median = ela_bins[np.argmax(histogram, axis=0)]

    error = np.average(np.abs((elas_reshaped - elas_median)), weights=histogram, axis=0)

    mappable = mpl.cm.ScalarMappable(mpl.colors.Normalize(100, 800), cmap="RdBu_r")

    slope_norm = 1 - np.repeat((np.clip(slope_ish / 2, 0, 1)).reshape(slope_ish.shape + (1,)), 4, axis=-1)
    slope_norm[:, :, -1] = 1.


    plt.subplot(131)
    plt.imshow(mappable.to_rgba(elas_highres) * slope_norm, extent=[bounds.left, bounds.right, bounds.bottom, bounds.top])
    plt.colorbar(mappable, ax=plt.gca())

    plt.subplot(132)
    vals = mappable.to_rgba(elas_highres) * slope_norm * np.repeat((dem_highres > elas_highres).astype(float).reshape(slope_ish.shape + (1,)), 4, axis=-1)
    plt.imshow(vals)

    plt.subplot(133)
    plt.imshow(error)
    
    plt.show()



def climate(lapse_rate=-0.005):

    rgi = gpd.read_file("zip://../../UiO/GlobalSurgeDetection/data/rgi/rgi6/nsidc0770_07_rgi60_Svalbard.zip/07_rgi60_Svalbard.shp")

    with xr.open_zarr("/home/erikmann/Projects/UiO/GlobalSurgeDetection/.cache/interpret_stack-REGN79E021X24Y05.zarr") as data:
        temp = data["air_temperature"].mean("year")
        res = np.diff(temp["x_era"])[0]

        shape = temp.shape

        left = float(temp["x_era"].min()) - res / 2
        upper = float(temp["y_era"].max()) + res / 2

        transform = rio.transform.from_origin(left, upper, res, res)
        with rio.open("input/S0_DTM20.tif") as raster:
            dem_highres = raster.read(1, masked=True).filled(0)
            highres_transform = raster.transform
            dem = np.empty(shape, dtype="float32")
            crs = raster.crs

            dem = rasterio.warp.reproject(
                dem_highres,
                destination=dem,
                src_crs=crs,
                dst_crs=crs,
                src_transform=highres_transform,
                dst_transform=transform,
                resampling=rasterio.warp.Resampling.bilinear
            )[0]

            temp -= dem * lapse_rate
            del dem


        temp_highres = np.empty(dem_highres.shape)

        temp_highres = rasterio.warp.reproject(
            temp.values,
            destination=temp_highres,
            src_transform=transform,
            dst_transform=highres_transform,
            src_crs=crs,
            dst_crs=crs,
            resampling=rasterio.warp.Resampling.bilinear,
        )

        temp_highres += dem_highres * lapse_rate

    with rio.open("temp.tif", "w", driver="GTiff", width=temp_highres.shape[1], height=temp_highres.shape[0], count=1, dtype="float32", compress="deflate", crs=crs, transform=highres_transform) as raster:
        raster.write(1, temp_highres)

 


def main():
    dates = read_and_format_db()

    distribution = generate_fit_distribution(dates=dates)

    region = get_regions()["nl_bigger"]

    show_map(distribution, bounds=region, age=1270.)



if __name__ == "__main__":
    main()